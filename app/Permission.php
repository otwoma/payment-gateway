<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends \Spatie\Permission\Models\Permission
{
    public static function defaultPermissions()
    {
        $array = array(
            'viewUser' => 'is able to view users',
            'addUser' => 'is able to add users',
            'editUser' => 'is able to edit users',
            'deleteUser' => 'is able to delete users',

            'viewRole' => 'is able to view roles',
            'addRole' => 'is able to add roles',
            'editRole' => 'is able to edit roles',
            'deleteRole' => 'is able to delete roles',

            'viewPermission' => 'is able to view permissions',
            'addPermission' => 'is able to add permissions',
            'editPermission' => 'is able to edit permissions',
            'deletePermission' => 'is able to delete permissions',

            'viewConfiguration' => 'is able to view configuration menu',
            'viewUserManagement' => 'is able to view user management menu',
            'viewClientUsers' => 'is able to view client users menu',
            'viewFinancialInstitution' => 'is able to view financial institution menu'

        );

        return $array;
    }
}
