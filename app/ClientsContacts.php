<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientsContacts extends Model
{
    protected $table = 'clients_contacts';
    protected $guarded = [];
}
