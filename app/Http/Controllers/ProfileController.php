<?php

namespace App\Http\Controllers;

use App\PasswordHistory;
use App\Role;
use App\Traits\UploadTrait;
use Gate;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        $role = Role::all();
        return view('profile.edit', compact('role'));
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            'avatar' => 'required|image|mimes:jpeg,jpg,png,gif,svg|max:2048'
        ]);

        $imageName = time().'.'.$request->avatar->extension();

        $request->avatar->move(public_path('images'), $imageName);

        flash('Avatar successfully uploaded')->success();
        return redirect()->back();
    }

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProfileRequest $request)
    {
        auth()->user()->update($request->all());

        flash('Profile successfully updated')->success();
        return back();
    }

    /**
     * Change the password
     *
     * @param  \App\Http\Requests\PasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(PasswordRequest $request)
    {
        $user = Auth::user();
        $passwordHistories = $user->passwordHistories()->take(env('PASSWORD_HISTORY_NUM'))->get();

        foreach ($passwordHistories as $passHistory) {
            echo $passHistory->password;
            if (hash::check($request->get('password'), $passHistory->password)) {
                flash('Your new password can not be the same as any of your recent passwords. Please choose a new password.')->info();
                return redirect()->back();
            }
        }

        auth()->user()->update(['password' => Hash::make($request->get('password'))]);

        $passwordHistory = PasswordHistory::create([
            'user_id' => $user = user()->id,
            'password' => bcrypt($request->get('password'))
        ]);

        flash('Password successfully updated.')->success();

        return back();
    }
}
