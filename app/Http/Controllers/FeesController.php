<?php

namespace App\Http\Controllers;

use App\FeeGroups;
use Carbon\Carbon;
use Dotenv\Validator;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class FeesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fees = FeeGroups::all();
        return view('fees_configuration.index', compact('fees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'group_name' => 'required',
            'fee' => 'required',
            'percent' => 'required'
        ]);

        if ($validator->passes()) {
            $feeGroup = new FeeGroups();
            $feeGroup->group_name = ucfirst($request->input('group_name'));
            $feeGroup->fee = $request->input('fee');
            $feeGroup->percent = $request->input('percent');
            $feeGroup->created_at = Carbon::now();
            $feeGroup->updated_at = Carbon::now();
            $feeGroup->save();

            flash('Fee group added successfully')->success();
            return redirect()->route('fees_configuration.index');

        } else {
            $messages = $validator->errors();
            foreach ($messages->all() as $message) {
                flash('Validation error!'.''. $message)->error();
            }

            return redirect()->back()->withInput(Input::all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fee = FeeGroups::find($id);
        return view('fees_configuration.edit', compact('fee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
