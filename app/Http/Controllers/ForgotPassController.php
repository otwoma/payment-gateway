<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\PasswordReset;
use Illuminate\Http\Request;
use App\Mail\VerifyPassReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPassController extends Controller
{
    use SendsPasswordResetEmails;

    public function __construct(User $user)
    {
        $this-> user = $user;
    }

    public function showRequestForm()
    {
        return view('auth.email');
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            flash('User does not exist.')->error();
            return redirect()->back();
        }

        $passwordReset = PasswordReset::create([
            'user_id' => $user->id,
            'email' => $user->email,
            'token' => str_random(60),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        Mail::to($user->email)->send(new VerifyPassReset($user));

        flash('A password reset link has been sent to your email')->info();
        return redirect()->route('login');
    }

    public function verifyPassRequest($token)
    {
        $verifyPassReq = PasswordReset::where('token', $token)->latest()->firstOrFail();
        $user = $verifyPassReq->user;

        if (!$user->email) {
            flash('Invalid password request. User not found.');
            return redirect()->route('login');
        }
        return view('auth.reset-pass');
    }

    public function resetPass(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $token = $request->token;
        $password = $request->password;
        $tokenData = PasswordReset::where('token', $token)->first();

        $user = User::where('email', $tokenData)->first();

        if (!$user['email']) {
            flash('User not found')->error();
            return redirect()->route('login');
        }

        $user->password = Hash::make($password);
        $user->update();

//        Auth::login($user);

        PasswordReset::where('email', $user->email)->delete();

        flash('Your password has been changed. Please login.')->info();
        return redirect()->route('login');
    }
}
