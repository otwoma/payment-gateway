<?php

namespace App\Http\Controllers\Auth;

use App\Otp;
use App\Role;
use App\User;
use Validator;
use Carbon\Carbon;
use App\VerifyUser;
use App\Mail\VerifyMail;
use App\PasswordHistory;
use App\PasswordSecurity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|max:255|unique:users',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone_number' => 'required',
            'password' => 'required|min:6',
            'password_confirm' => 'required|same:password'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        $role = Role::where('name', 'User')->first();

        $user = User::create([
            'username' => $data['username'],
            'first_name' => ucfirst($data['first_name']),
            'last_name' => ucfirst($data['last_name']),
            'email' => $data['email'],
            'phone_number' => $data['phone_number'],
            'isVerified' => 0,
            'password' => bcrypt($data['password']),
            'active' => 1,
        ]);

        $user->assignRole($role->name);

        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);

        $passwordSecurity = PasswordSecurity::create([
            'user_id' => $user->id,
            'password_expiry_days' => 30,
            'password_updated_at' => Carbon::now(),
        ]);

        $passwordHistory = PasswordHistory::create([
            'user_id' => $user->id,
            'password' => bcrypt($data['password'])
        ]);

        $userOTP = Otp::create([
            'user_id' => $user->id,
            'otp' => mt_rand(100000, 999999),
        ]);

        //TODO implement event listener on password reset

        Mail::to($user->email)->send(new VerifyMail($user));

        return $user;
    }

    public static function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->latest()->firstOrFail();
            $user = $verifyUser->user;

            if (!$user->email_verified_at) {
                $user->email_verified_at = Carbon::now();
                $user->save();

                flash('Your email has been verified.')->info();
            } else {
                flash('Your email is already verified.')->info();
            }
        return redirect('login');
    }

    protected function registered(Request $request, $user)
    {
        $this->guard()->logout();
        flash('Kindly check your mail for an email verification link.');
        return redirect('login');
    }
}
