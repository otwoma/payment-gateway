<?php

namespace App\Http\Controllers;

use App\Otp;
use App\PasswordHistory;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\Console\Input\Input;

class AuthController extends Controller
{
    use AuthenticatesUsers;


    public function login()
    {

        return view('login');
    }

    public function loginProcess(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $remember = $request->has('remember') ? true : false;

        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (Auth::attempt($credentials, $remember)) {

            $user = user();

            if ($user->active == 0) {
                auth()->logout();
                flash('Please update/change password your before you continue.')->warning();
                return redirect()->route('change-password');
            }

            if ($user->isVerified == 0) {
                auth()->logout();
                //TODO send OTP method call
                flash('Validate your phone number before you log in.');
                return view('otp', compact('user'));
            }

            flash('Welcome')->success();

            return redirect()->intended(route('home'));
        }


        flash('Incorrect Credentials')->error();
        return redirect()->back()->withInput();


    }

    public function authenticated(Request $request, $user)
    {
        if (!$user->email_verified_at) {
            auth()->logout();
            flash('You need to verify your email first. A link has been sent to your email.')->warning();
        }

        $request->session()->forget('password_expired_id');

        $password_updated_at = $user->passwordSecurity->password_updated_at;
        $password_expiry_days = $user->passwordSecurity->password_expiry_days;
        $password_expiry_at = Carbon::parse($password_updated_at)->addDays($password_expiry_days);

        if ($password_expiry_at->lessThan(Carbon::now())) {
            $request->session()->put('password_expired_id', $user->id);
            auth()->logout();

            flash('Your Password is expired, You need to change your password')->info();

            return redirect('/passwordExpiration');
        }

        return redirect()->intended($this->redirectPath());
    }

    public function passChangeForm()
    {
        return view('change-password');
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required',
            'password_confirmation' => 'required|same:password'
        ]);

        $user = user();

        User::find(auth()->$user->id)->update([
            'password' => Hash::make($request->get('password')),
            'active' => user()->active = 1
        ]);

        $passwordHistory = PasswordHistory::create([
            'user_id' => $user->id,
            'password' => bcrypt($request->get('password')),
        ]);

        flash('Password updated successfully')->success();

        return redirect()->route('login');
    }

    public function verifyOTP(Request $request)
    {

        $this->validate($request, [
            'otp' => 'required'
        ]);

        /*TODO:: error handle if the otp code is incorrect*/

        $verifyOTP = Otp::where('otp', $request->get('otp'))->where('user_id', $request->get('id'))->latest()->first();
        $user = $verifyOTP->user;

        if ($user->isVerified == 0) {
            $user->isVerified = 1;

            flash('Phone Number has been verified')->success();
            auth()->login($user);
            Otp::where('otp', $user->id)->delete();
        }
        return view('home');
    }

    public function logout()
    {

        logout_all_guards();

        flash(__("Successfully logged out"))->info();

        return redirect()->route('login');
    }


}
