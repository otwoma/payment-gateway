<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PwdExpirationController extends Controller
{

    public function showPasswordExpirationForm(Request $request)
    {
        $password_expired_id = $request->session()->get('password_expired_id');

        if (!isset($password_expired_id)) {
            return redirect('login');
        }

        return view('auth.expired-password');
    }

    public function postPasswordExpiration(Request $request)
    {

        $this->validate($request, [
            'current_password' => 'required',
            'new_password' => 'required|string|min:6|confirmed',
        ]);

        $password_expired_id = $request->session()->get('password_expired_id');

        if (!isset($password_expired_id)) {
            return redirect('login');
        }

        $user = User::find($password_expired_id);

        if (!(Hash::check($request->get('current_password'), $user->password))) {
            // The passwords matches
            flash('Your current password does not matches with the password you provided. Please try again.')->error();
            return redirect()->back();
        }

        if (strcmp($request->get('current_password'), $request->get('new_password')) == 0) {
            //Current password and new password are same
            flash('New Password cannot be same as your current password. Please choose a different password.')->error();
            return redirect()->back();
        }

        //Change Password
        $user->password = Hash::make($request->new_password);
        $user->save();

        //Update password timestamp
        $user->passwordSecurity->password_updated_at = Carbon::now();
        $user->passwordSecurity->save();

        flash('Password changed successfully, You can now login')->success();
        return redirect()->route('login');
    }
}