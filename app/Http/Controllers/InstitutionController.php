<?php

namespace App\Http\Controllers;

use App\Role;
use Carbon\Carbon;
use App\Institution;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class InstitutionController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $institutions = Institution::all();
        return view('institution.index', compact('institutions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('institution.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'name' => 'required|unique:institutions',
            'company_reg_no' => 'required|max:15|unique:institutions',
            'kra_pin' => 'required|max:11|unique:institutions',
            'description' => 'required|max:255',
            'email' => 'required|email|unique:institutions',
            'phone_number' => 'required|max:10|unique:institutions',
            'postal_address' => 'required|max:11',
            'physical_address' => 'required|max:11|unique:institutions',

        ]);

        if ($validator->passes()) {
            // save institution
            $institution = new Institution();
            $institution->name = $request->input('name');
            $institution->company_reg_no = $request->input('company_reg_no');
            $institution->kra_pin = $request->input('kra_pin');
            $institution->description = $request->input('description');
            $institution->email = $request->input('email');
            $institution->phone_number = $request->input('phone_number');
            $institution->postal_address = $request->input('postal_address');
            $institution->physical_address = $request->input('physical_address');
            $institution->status = 0;
            $institution->active = 1;
            $institution->maker = Auth::user()->id;
            $institution->checker = '';
            $institution->created_at = Carbon::now();
            $institution->updated_at = Carbon::now();
            $institution->save();
    
            flash('Institution added successfully')->success();
            return redirect()->route('institution.index');
            
        } else {
            # validation failed
            $messages = $validator->errors();
            foreach ($messages->all() as $message) {
                flash('validation error! ' . '' . $message)->error();
            }
            return redirect()->back()->withInput(Input::all());
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function show(Institution $institution)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $institution = Institution::find($id);
        return view('institution.edit', compact('institution', $institution));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $institution = Institution::findOrFail($id);

        $institution->name = $request->input('name');
        $institution->company_reg_no = $request->input('company_reg_no');
        $institution->kra_pin = $request->input('kra_pin');
        $institution->description = $request->input('description');
        $institution->email = $request->input('email');
        $institution->phone_number = $request->input('phone_number');
        $institution->postal_address = $request->input('postal_address');
        $institution->physical_address = $request->input('physical_address');
        $institution->updated_at = Carbon::now();
        $institution->save();

        flash('Institution successfully updated.')->success();
        return redirect()->route('institution.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Institution  $institution
     * @return \Illuminate\Http\Response
     */
    public function destroy(Institution $institution)
    {
        //
    }
}
