<?php

namespace App\Http\Controllers;

use App\Client;
use App\ClientsContacts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('client.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // upload document
        if ($request->hasFile('upload_file')) {
            $data = $request->input('upload_file');
            $document = uniqid().'-'.$request->file('upload_file')->getClientOriginalName();
            $destination = base_path() . '/public/uploads';
            $request->file('upload_file')->move($destination, $document);
        }

        // save in clients
        $client = new Client();
        $client->business_name = $request->bname;
        $client->business_reg_no = $request->reg_no;
        $client->description = $request->desc;
        $client->phone_number = $request->mobile_number;
        $client->email = $request->email;
        $client->kra_pin = $request->kra_pin;
        $client->postal_address = $request->address;
        $client->document_name = $document;
        $client->active = 1;
        $client->status = 1;
        $client->maker = Auth::user()->id;
        $client->save();

        //clients-contacts
        $client_contact = new ClientsContacts();
        $client_contact->client_id =  $client->id;
        $client_contact->first_name = $request->fname;
        $client_contact->surname = $request->surname;
        $client_contact->other_name = $request->other_name;
        $client_contact->email = $request->c_email;
        $client_contact->id_type = $request->id_type;
        $client_contact->id_number = $request->id_number;
        $client_contact->phone_number = $request->c_mobile_number;
        $client_contact->postal_address = $request->c_address;
        $client_contact->maker = Auth::user()->id;
        $client_contact->active = 1;
        $client_contact->status = 1;
        $client_contact->save();

        // update users that details have been captured
        // $user = User::findOrFail(Auth::user()->id);
        // // $user->update_details = 0;
        // $user->save();

        return redirect()->route('client.index')->with('success', 'Client details recorded successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updateClientDetails()
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        //
    }
}
