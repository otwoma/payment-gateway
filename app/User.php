<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'first_name', 'last_name', 'email', 'password', 'active', 'avatar', 'isVerified', 'phone_number'
    ];

    protected $attributes = [
        'status' => 0, 'active' => 0, 'maker' => 0, 'checker' => 0,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = (empty($value) || strlen($value) > 58) ? $value : bcrypt($value);
    }

    public function passwordSecurity()
    {
        return $this->hasOne('App\PasswordSecurity');
    }

    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }

    public function verifyOTP()
    {
        return $this->hasOne('App\Otp');
    }

    public function passwordHistories()
    {
        return $this->hasMany('App\PasswordHistory');
    }

    public function getImageAttribute()
    {
        return $this->avatar;
    }

}
