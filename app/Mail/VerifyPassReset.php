<?php

namespace App\Mail;

use App\PasswordReset;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyPassReset extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this-> user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tokenRecord = PasswordReset::where('user_id', $this->user->id)->latest()->firstOrFail();

        return $this->view('auth.reset')
            ->with([
                'email' => $this->user->email,
                'token' => $tokenRecord->token
            ]);
    }
}
