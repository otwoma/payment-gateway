<?php

namespace App\Mail;

use App\User;
use App\VerifyUser;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VerifyMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $token_record = VerifyUser::where('user_id', $this->user->id)->latest()->firstOrFail();

        return $this->view('emails.verifyUser')
            ->with([
                'name' => $this->user->name,
                'email' => $this->user->email,
                'token' => $token_record->token
            ]);
    }
}
