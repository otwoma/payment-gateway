<?php


use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserRolePermissionTableSeeder::class,
        ]);

        factory(App\Institution::class, 10)->create();
        factory(App\FeeGroups::class, 5)->create();
    }
}