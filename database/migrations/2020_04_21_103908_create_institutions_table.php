<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstitutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institutions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('company_reg_no')->unique();
            $table->string('kra_pin')->unique();
            $table->string('description', 255);
            $table->string('email')->unique();
            $table->string('phone_number')->unique();
            $table->string('postal_address');
            $table->string('physical_address');
            $table->tinyInteger('status');
            $table->tinyInteger('active');
            $table->bigInteger('maker');
            $table->bigInteger('checker');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('institutions');
    }
}
