<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'username' => $faker->unique()->userName,
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'phone_number' => $faker->unique()->e164PhoneNumber,
        'password' => $password ?: $password = bcrypt('secret'),
        'isVerified' => 1,
        'status' => 1,
        'active' => 1,
        'maker' => 1,
        'checker' => 1,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Institution::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->company,
        'company_reg_no' => $faker->swiftBicNumber,
        'kra_pin' => $faker->swiftBicNumber,
        'description' => $faker->sentence,
        'email' => $faker->unique()->companyEmail,
        'phone_number' => $faker->unique()->e164PhoneNumber,
        'postal_address' => $faker->address,
        'physical_address' => $faker->streetAddress,
        'status' => 1,
        'active' => 1,
        'maker' => 1,
        'checker' => 1,
    ];
});

$factory->define(App\FeeGroups::class, function (Faker\Generator $faker) {
    return [
        'group_name' => $faker->city,
        'fee' => $faker->numberBetween(2000, 10000),
        'percent' => $faker->randomDigit(),
    ];
});
