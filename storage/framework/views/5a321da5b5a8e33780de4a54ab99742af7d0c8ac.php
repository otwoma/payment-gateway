<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo e(asset('images/payconnect-logo.png')); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
	

    <title>SWIFT-PAY - Payment Gateway</title>

    <link href="<?php echo e(URL::asset('css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(URL::asset('font-awesome/font-awesome.css')); ?>" rel="stylesheet">

    <link href="<?php echo e(URL::asset('css/animate.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(URL::asset('css/style.css')); ?>" rel="stylesheet">

    <script src="<?php echo e(asset('js/jquery-latest.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/passchecker.js')); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
    
 
</head>

<body class="gray-bg">
<style>
    input{
        border:none;
        padding:8px;
    }
    #ppbar{
        background:#f3f3f4;
        width:300px;
        height:15px;
        margin:5px;
    }
    #pbar{
        margin:0px;
        width:0px;
        background:lightgreen;
        height: 100%;
    }
    #ppbartxt{
        text-align:right;
        margin:2px;
    }
</style>
<div class="content">
    
    <div class="loginColumns animated fadeInDown">
        <?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row justify-content-center">

            <div class="col-md-6">
                <div>
                    <div>
                        <h2 class="font-bold">Welcome to SWIFT-PAY</h2>
                    </div>
                    <div class="">
                        <img src="<?php echo e(asset('images/paycon_img.png')); ?>" alt="logo" style="width: 350px; height: 150px;">
                        <hr>
                        <p>
                            Perfectly designed and precisely prepared Payment Gateway to suit our customers needs.
                            Join us today to experience real time and efficient money transfer.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="ibox-content">

                    <h3 class="m-t-none m-b">Sign up</h3>
                    <p>Sign in today for more experience.</p>

                    <form role="form" method="POST" action="<?php echo e(route('register')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="form-group">
                            <label for="username" class="control-label">Username</label>

                            <input id="user" type="text" class="form-control <?php echo e($errors->has('username') ? ' is-invalid' : ''); ?>" name="username" value="<?php echo e(old('username')); ?>"
                                  required autofocus>

                            <?php if($errors->has('username')): ?>
                                <span class="invalid-feedback">
                                    <strong><?php echo e($errors->first('username')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group">
                            <label for="firstname" class="control-label">First Name</label>

                            <input type="text" class="form-control"
                                   <?php echo e($errors->has('firstname') ? ' is-invalid' : ''); ?> name="first_name"
                                   value="<?php echo e(old('first_name')); ?>"
                                   required autofocus>

                            <?php if($errors->has('firstname')): ?>
                                <span class="invalid-feedback">
                                    <strong><?php echo e($errors->first('firstname')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group">
                            <label for="lastname" class="control-label">Last Name</label>

                            <input type="text" class="form-control"
                                   <?php echo e($errors->has('lastname') ? ' is-invalid' : ''); ?> name="last_name"
                                   value="<?php echo e(old('last_name')); ?>"
                                   required autofocus>

                            <?php if($errors->has('lastname')): ?>
                                <span class="invalid-feedback">
                                    <strong><?php echo e($errors->first('lastname')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group">
                            <label for="email" class="control-label">Email Address</label>

                            <input id="email" type="email" class="form-control <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>"
                                   required autofocus>

                            <?php if($errors->has('email')): ?>
                                <span class="invalid-feedback">
                                    <strong><?php echo e($errors->first('email')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group">
                            <label for="phoneNumber" class="control-label">Phone Number</label>

                            <input type="text" class="form-control <?php echo e($errors->has('phoneNumber') ? ' is-invalid' : ''); ?>" name="phone_number"value="<?php echo e(old('phone_number')); ?>"
                                   required autofocus>

                            <?php if($errors->has('phoneNumber')): ?>
                                <span class="invalid-feedback">
                                    <strong><?php echo e($errors->first('phoneNumber')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group">
                            <label for="password" class="control-label">Password</label>

                            <input id="pass" type="password" class="form-control <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" placeholder="******" required>

                            <?php if($errors->has('password')): ?>
                                <span class="invalid-feedback">
                                    <strong><?php echo e($errors->first('password')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>

                        <div id="ppbar" title="Strength"><div id="pbar"></div></div>
                        <div id="ppbartxt"></div>

                        <div class="form-group">
                            <label for="password" class="control-label">Confirm Password</label>

                            <input id="pass2" type="password" class="form-control" name="password_confirm" placeholder="******" required>
                        </div>
                        <div>
                            <button class="btn  btn-primary float-right " type="submit">
                                <strong>Register</strong>
                            </button>
                        </div>
                    </form>

                    <p class="text-muted text-center">
                        <a href="<?php echo e(route('login')); ?>" class="btn btn-link">Already have an account? Login.</a>
                    </p>
                    <p class="text-muted text-center">
                        <small>SWIFT-PAY &copy; <?php echo date("Y")?></small>
                    </p>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                <strong>PayConnect Limited</strong>
            </div>
            <div class="col-md-6 text-right">
                <strong>
                    <small>2020-<?php echo date("Y");?></small>
                </strong>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<?php /**PATH C:\laragon\www\payment-gateway\resources\views/auth/register.blade.php ENDPATH**/ ?>