<?php $__env->startSection('title', 'Institution'); ?>

<?php $__env->startSection('content'); ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Financial Institutions</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo e(url('home')); ?>">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Institution Configuration</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5><i class="fa fa-plus-square-o"></i> Edit Institution</h5>
                        <div class="ibox-tools">
                            <a href="<?php echo e(route('institution.index')); ?>" data-toggle="modal" rel="tooltip"
                               data-placement="top" title="Back to Institution">
                                <i class="fa fa-bank"></i>
                                <strong>Institution</strong>
                            </a>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <?php echo Form::model($institution, array('route' => array('institution.update', $institution->id), 'method' => 'PUT')); ?>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group <?php if($errors->has('companyName')): ?> has-error <?php endif; ?>">
                                    <?php echo Form::label('companyName', 'Company Name'); ?>

                                    <?php echo Form::text('name', null, array('class' => 'form-control')); ?>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group <?php if($errors->has('registrationNumber')): ?> has-error <?php endif; ?>">
                                    <?php echo Form::label('registrationNumber', 'Registration Number'); ?>

                                    <?php echo Form::text('company_reg_no', null, array('class' => 'form-control')); ?>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group <?php if($errors->has('email')): ?> has-error <?php endif; ?>">
                                    <?php echo Form::label('email', 'Email Address'); ?>

                                    <?php echo Form::text('email', null, array('class' => 'form-control')); ?>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group <?php if($errors->has('phoneNumber')): ?> has-error <?php endif; ?>">
                                    <?php echo Form::label('phoneNumber', 'Phone Number'); ?>

                                    <?php echo Form::text('phone_number', null, array('class' => 'form-control')); ?>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group <?php if($errors->has('krapin')): ?> has-error <?php endif; ?>">
                                    <?php echo Form::label('krapin', 'KRA PIN'); ?>

                                    <?php echo Form::text('kra_pin', null, array('class' => 'form-control')); ?>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group <?php if($errors->has('physicalAddress')): ?> has-error <?php endif; ?>">
                                    <?php echo Form::label('physicalAddress', 'Physical Address'); ?>

                                    <?php echo Form::text('physical_address', null, array('class' => 'form-control')); ?>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group <?php if($errors->has('postalAddress')): ?> has-error <?php endif; ?>">
                                    <?php echo Form::label('postalAddress', 'Postal Address'); ?>

                                    <?php echo Form::text('postal_address', null, array('class' => 'form-control')); ?>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group <?php if($errors->has('postalAddress')): ?> has-error <?php endif; ?>">
                                    <?php echo Form::label('description', 'Description'); ?>

                                    <?php echo Form::textarea('description', null, array('class' => 'form-control')); ?>

                                </div>
                            </div>
                        </div>
                        <?php echo e(Form::submit('Update', array('class' => 'btn btn-primary'))); ?>


                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\payment-gateway\resources\views/institution/edit.blade.php ENDPATH**/ ?>