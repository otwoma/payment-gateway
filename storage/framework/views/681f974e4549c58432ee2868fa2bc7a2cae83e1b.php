<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo e(asset('images/payconnect-logo.png')); ?>">

    <title>SWIFT-PAY - Payment Gateway</title>

    <link href="<?php echo e(URL::asset('css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(URL::asset('font-awesome/font-awesome.css')); ?>" rel="stylesheet">

    <link href="<?php echo e(URL::asset('css/animate.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(URL::asset('css/style.css')); ?>" rel="stylesheet">
</head>

<body class="gray-bg">
<div class="content">
    <div class="loginColumns animated fadeInDown">
        <?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <div class="row">
            <div class="col-md-6">
                <div>
                    <div>
                        <h2 class="font-bold">Welcome to SWIFT-PAY</h2>
                    </div>
                    <div class="">
                        <img src="<?php echo e(asset('images/paycon_img.png')); ?>" alt="logo" style="width: 350px; height: 150px;">
                        <hr>
                        <p>
                            Perfectly designed and precisely prepared Payment Gateway to suit our customers needs.
                            Join us today to experience real time and efficient money transfer.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="ibox-content">

                    <h3 class="m-t-none m-b">Sign in</h3>
                    <p>Sign in today for more experience.</p>

                    <form role="form" method="POST" action="<?php echo e(route('login')); ?>">
                        <?php echo csrf_field(); ?>

                        <div class="form-group">
                            <label for="email" class="control-label">Email</label>

                            <input id="email" type="email" class="form-control <?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" value="<?php echo e(old('email')); ?>" required autofocus>

                            <?php if($errors->has('email')): ?>
                                <span class="invalid-feedback">
                                    <strong><?php echo e($errors->first('email')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>

                        <div class="form-group">
                            <label for="password" class="control-label">Password</label>

                            <input id="password" type="password" class="form-control <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" required>

                            <?php if($errors->has('password')): ?>
                                <span class="invalid-feedback">
                                    <strong><?php echo e($errors->first('password')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>

                        <div class="checkbox">
                            <label class="float-right">
                                <input type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?> class="i-checks"> Remember Me
                            </label>
                        </div>

                        <div>
                            <button class="btn  btn-primary float-right " type="submit"><i class="fa fa-paper-plane"></i>
                                <strong>Log in</strong>
                            </button>
                            <a href="<?php echo e(route('email')); ?>" class="btn btn-link pull-right">Forgot your password?</a>
                        </div>
                    </form>
                    <p class="text-muted text-center">
                        <a href="<?php echo e(route('register')); ?>" class="btn btn-link">Don't have an account? Register.</a>
                    </p>

                    <p class="text-muted text-center">
                        <small>SWIFT-PAY &copy; <?php echo date("Y")?></small>
                    </p>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                <strong>PayConnect Limited</strong>
            </div>
            <div class="col-md-6 text-right">
                <strong>
                    <small>2020-<?php echo date("Y");?></small>
                </strong>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<?php /**PATH C:\laragon\www\payment-gateway\resources\views/login.blade.php ENDPATH**/ ?>