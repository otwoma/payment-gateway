<?php $__env->startSection('title', ' Profile'); ?>

<?php $__env->startSection('content'); ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>User Profile</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo e(url('home')); ?>">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    Settings
                </li>
                <li class="breadcrumb-item active">
                    <strong>Profile</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row">
            <div class="col-md-8">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Edit Profile</h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" action="<?php echo e(route('profile.upload')); ?>" autocomplete="off"
                              enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('put'); ?>
                            <div class="form-group">
                                <input type="file" class="form-control-file" name="avatar" id="avatarFile"
                                       aria-describedby="fileHelp">
                                
                                
                                
                                <small id="fileHelp" class="form-text text-muted">
                                    Please upload a valid image file. Size of image should not be more than 2MB.
                                </small>
                            </div>
                            <div class="dt-button-info">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i>
                                    Upload Photo
                                </button>
                            </div>
                            <br>
                        </form>
                        <form method="post" action="<?php echo e(route('profile.update')); ?>" autocomplete="off"
                              enctype="multipart/form-data">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('put'); ?>
                            <div class="row">

                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label><?php echo e(__(" Username")); ?></label>
                                        <input type="text" name="username" class="form-control"
                                               value="<?php echo e(old('name', auth()->user()->username)); ?>">
                                        <?php echo $__env->make('alerts.feedback', ['field' => 'name'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"><?php echo e(__(" Email address")); ?></label>
                                        <input type="email" name="email" class="form-control" placeholder="Email"
                                               value="<?php echo e(old('email', auth()->user()->email)); ?>">
                                        <?php echo $__env->make('alerts.feedback', ['field' => 'email'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-footer ">
                                <button type="submit" class="btn btn-primary btn-round"><i
                                            class="fa fa-share"></i><?php echo e(__(' Save')); ?>

                                </button>
                            </div>
                        </form>
                    </div>
                    <br>

                    <div class="ibox-title">
                        <h5>Password</h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" action="<?php echo e(route('profile.password')); ?>" autocomplete="off">
                            <?php echo csrf_field(); ?>
                            <?php echo method_field('put'); ?>
                            <?php echo $__env->make('alerts.success', ['key' => 'password_status'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group <?php echo e($errors->has('password') ? ' has-danger' : ''); ?>">
                                        <label><?php echo e(__(" Current Password")); ?></label>
                                        <input class="form-control <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>"
                                               name="old_password" placeholder="<?php echo e(__('Current Password')); ?>"
                                               type="password" required>
                                        <?php echo $__env->make('alerts.feedback', ['field' => 'old_password'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group <?php echo e($errors->has('password') ? ' has-danger' : ''); ?>">
                                        <label><?php echo e(__(" New password")); ?></label>
                                        <input class="form-control <?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>"
                                               placeholder="<?php echo e(__('New Password')); ?>" type="password" name="password"
                                               required>
                                        <?php echo $__env->make('alerts.feedback', ['field' => 'password'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group <?php echo e($errors->has('password') ? ' has-danger' : ''); ?>">
                                        <label><?php echo e(__(" Confirm New Password")); ?></label>
                                        <input class="form-control" placeholder="<?php echo e(__('Confirm New Password')); ?>"
                                               type="password" name="password_confirmation" required>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-footer ">
                                <button type="submit" class="btn btn-primary btn-round ">
                                    Change Password
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>User Photo</h5>
                    </div>
                    <div class="ibox-content">
                        <a href="#">
                            <img class="card-img-bottom" src="<?php echo e(asset(auth()->user()->avatar)); ?>" alt="...">
                        </a>
                        <div>
                            <label for="email">Username:</label>
                            <?php echo e(auth()->user()->username); ?>

                        </div>

                        <div>
                            <label for="email">Email:</label>
                            <?php echo e(auth()->user()->email); ?>

                        </div>
                        <div>
                            <label for="role">Role:</label>
                            <?php echo e(user()->roles()->pluck('name')); ?>

                        </div>

                        
                            
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\payment-gateway\resources\views/profile/edit.blade.php ENDPATH**/ ?>