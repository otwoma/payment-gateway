<?php $__env->startSection('title', 'Fee Configurations'); ?>

<?php $__env->startSection('content'); ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Users</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo e(url('home')); ?>">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Edit Fees Group</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5><i class="fa fa-plus-square-o"></i> Edit Fee Group</h5>
                        <div class="ibox-tools">
                            <a href="<?php echo e(route('fees_configuration.index')); ?>" data-toggle="modal" rel="tooltip"
                               data-placement="top" title="Back to List">
                                <i class="fa fa-money"></i>
                                <strong>Fee Groups</strong>
                            </a>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <?php echo Form::model($fee, array('route' => array('fees_configuration.update', $fee->id), 'method' => 'PUT')); ?>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group <?php if($errors->has('groupName')): ?> has-error <?php endif; ?>">
                                    <?php echo Form::label('groupName', 'Group Name'); ?>

                                    <?php echo Form::text('group_name', null, array('class' => 'form-control')); ?>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group <?php if($errors->has('feeCharge')): ?> has-error <?php endif; ?>">
                                    <?php echo Form::label('feeCharge', 'Fees'); ?>

                                    <?php echo Form::text('fee', null, array('class' => 'form-control')); ?>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group <?php if($errors->has('percentage')): ?> has-error <?php endif; ?>">
                                    <?php echo Form::label('percentage', 'Percentage'); ?>

                                    <?php echo Form::number('percent'), null, array('class' => 'form-control'); ?>

                                </div>
                            </div>
                        </div>
                        <?php echo e(Form::submit('Update', array('class' => 'btn btn-primary'))); ?>


                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\payment-gateway\resources\views/fees_configuration/edit.blade.php ENDPATH**/ ?>