<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo e(asset('images/payconnect-logo.png')); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />


    <title>SWIFT-PAY - Payment Gateway</title>

    <link href="<?php echo e(URL::asset('css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(URL::asset('font-awesome/font-awesome.css')); ?>" rel="stylesheet">

    <link href="<?php echo e(URL::asset('css/animate.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(URL::asset('css/style.css')); ?>" rel="stylesheet">

    <script src="<?php echo e(asset('js/jquery-latest.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/passchecker.js')); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>


</head>

<body class="gray-bg">
<style>
    input{
        border:none;
        padding:8px;
    }
    #ppbar{
        background:#f3f3f4;
        width:300px;
        height:15px;
        margin:5px;
    }
    #pbar{
        margin:0px;
        width:0px;
        background:lightgreen;
        height: 100%;
    }
    #ppbartxt{
        text-align:right;
        margin:2px;
    }
</style>
<div class="content">

    <div class="loginColumns animated fadeInDown">
        <?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row justify-content-center">

            <div class="col-md-6">
                <div>
                    <div>
                        <h2 class="font-bold">Welcome to SWIFT-PAY</h2>
                    </div>
                    <div class="">
                        <img src="<?php echo e(asset('images/paycon_img.png')); ?>" alt="logo" style="width: 350px; height: 150px;">
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="ibox-content">

                    <h3 class="m-t-none m-b">Verify Mobile Number</h3>

                    <form role="form" method="POST" action="<?php echo e(url('verifyOTP')); ?>">
                        <?php echo csrf_field(); ?>
                        <input type="hidden" name="id" value="<?php echo e($user ?? ''); ?>">

                        <div class="form-group">
                            <label for="otp" class="control-label">Enter OTP</label>

                            <input id="user" type="text" class="form-control <?php echo e($errors->has('otp') ? ' is-invalid' : ''); ?>" name="otp" value="<?php echo e(old('otp')); ?>"
                                   required autofocus>

                            <?php if($errors->has('otp')): ?>
                                <span class="invalid-feedback">
                                    <strong><?php echo e($errors->first('otp')); ?></strong>
                                </span>
                            <?php endif; ?>
                        </div>
                        <div>
                            <button class="btn  btn-primary float-right " type="submit">
                                <strong>Verify</strong>
                            </button>
                        </div>
                    </form>

                    <p class="text-muted text-center">
                        <small>SWIFT-PAY &copy; <?php echo date("Y")?></small>
                    </p>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                <strong>PayConnect Limited</strong>
            </div>
            <div class="col-md-6 text-right">
                <strong>
                    <small>2020-<?php echo date("Y");?></small>
                </strong>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<?php /**PATH C:\laragon\www\payment-gateway\resources\views/otp.blade.php ENDPATH**/ ?>