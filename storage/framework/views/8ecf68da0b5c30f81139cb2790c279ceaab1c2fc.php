<?php $__env->startSection('title', 'User Configuration'); ?>

<?php $__env->startSection('content'); ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Users</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo e(url('home')); ?>">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a>User Management</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>User Configuration</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Registered Users</h5>
                        <div class="ibox-tools">
                            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('addUser')): ?>
                                <a href="<?php echo e(route('users.create')); ?>" data-target="#addUsrModal" rel="tooltip"
                                   data-placement="top" title="Add User">
                                    <i class="fa fa-user-plus"></i>
                                    Add User
                                </a>
                            <?php endif; ?>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="userDataTable">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Creation Date</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($user->id); ?></td>
                                        <td><?php echo e($user->username); ?></td>
                                        <td><?php echo e($user->email); ?></td>
                                        <td><?php echo e($user->roles()->pluck('name')->implode(' ')); ?></td>
                                        <td><?php echo e($user->created_at->format('F d, Y h:ia')); ?></td>
                                        <td class="text-right">
                                            <?php if($user->id!=auth()->user()->id): ?>
                                                <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('editUser')): ?>
                                                    <a type="button" href="<?php echo e(route("users.edit", $user)); ?>" rel="tooltip"
                                                       class="btn btn-success btn-sm" data-placement="top"
                                                       title="Edit User">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                <?php endif; ?>
                                                <form action="<?php echo e(route('users.destroy', $user)); ?>" method="post"
                                                      style="display:inline-block;" class="delete-form">
                                                    <?php echo csrf_field(); ?>
                                                    <?php echo method_field('delete'); ?>
                                                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('deleteUser')): ?>
                                                        <button type="button" rel="tooltip"
                                                                class="btn btn-danger btn-sm" title="Delete User"
                                                                data-placement="top"
                                                                onclick="confirm('<?php echo e(__('Are you sure you want to delete this user?')); ?>') ? this.parentElement.submit() : ''">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    <?php endif; ?>
                                                </form>
                                            <?php else: ?>
                                                <a type="button" href="<?php echo e(route('profile.edit')); ?>" rel="tooltip"
                                                   class="btn btn-success btn-sm " data-original-title=""
                                                   title="Edit Profile">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Creation Date</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script>
        $(document).ready(function () {
            $('#userDataTable').DataTable();
        });
    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\payment-gateway\resources\views/users/index.blade.php ENDPATH**/ ?>