<?php $__env->startSection('title', 'Fee Configurations'); ?>

<?php $__env->startSection('content'); ?>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Users</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="<?php echo e(url('home')); ?>">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a>Configurations</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Fees Configuration</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <?php echo $__env->make('flash::message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <div class="row">
            <div class="col-md-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Fee Groups</h5>
                        <div class="ibox-tools">
                            <a href="#" data-toggle="modal" data-target="#addFeeModal" rel="tooltip" data-placement="top" title="Add Fee">
                                <i class="fa fa-money"></i>
                                Add Fee Group
                            </a>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="userDataTable">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Group Name</th>
                                    <th>Fee(s)</th>
                                    <th>% Charge</th>
                                    <th>Creation Date</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $fees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fee): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <td><?php echo e($fee->id); ?></td>
                                        <td><?php echo e($fee->group_name); ?></td>
                                        <td><?php echo e($fee->fee); ?></td>
                                        <td><?php echo e($fee->percent); ?></td>
                                        <td><?php echo e($fee->created_at->format('F d, Y h:ia')); ?></td>
                                        <td class="text-right">
                                            <?php if($fee->id!=auth()->user()->id): ?>
                                                <a href="<?php echo e(URL::to('fees_configuration/'.$fee->id.'/edit')); ?>" title="Edit Institution"
                                                   class="btn btn-success btn-sm" rel="tootltip" data-placement="top">
                                                    <i class="fa fa-edit"></i>
                                                </a>

                                                <form action="<?php echo e(route('fees_configuration.destroy', $fee)); ?>" method="post"
                                                      style="display:inline-block;" class="delete-form">
                                                    <?php echo csrf_field(); ?>
                                                    <?php echo method_field('delete'); ?>
                                                    <button type="button" rel="tooltip"
                                                            class="btn btn-danger btn-sm" title="Delete Group" data-placement="top"
                                                            onclick="confirm('<?php echo e(__('Are you sure you want to delete this group?')); ?>') ? this.parentElement.submit() : ''">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Group Name</th>
                                    <th>Fee(s)</th>
                                    <th>% Charge</th>
                                    <th>Creation Date</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="addFeeModal" class="modal fade animated fadeInDown" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-money"></i> New Fee Group</h4>
                </div>
                <div class="modal-body">
                    <form action="<?php echo e('fees_configuration.store'); ?>" method="POST">
                        <?php echo csrf_field(); ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="feeGroup">Group Name</label>
                                    <input type="text" class="form-control" name="group_name" id="feeGroup"
                                           value="<?php echo e(old('group_name')); ?>" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fee">Fee</label>
                                    <input type="number" class="form-control" name="fee" id="fee"
                                           value="<?php echo e(old('fee')); ?>" autocomplete="off" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="rate">Percentage</label>
                                    <input type="text" class="form-control" name="percent" id="percentage"
                                           autocomplete="off" value="<?php echo e(old('percentage')); ?>" required>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\laragon\www\payment-gateway\resources\views/fees_configuration/index.blade.php ENDPATH**/ ?>