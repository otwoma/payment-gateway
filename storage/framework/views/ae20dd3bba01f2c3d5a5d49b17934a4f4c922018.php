<div class="row border-bottom">
    <nav class="navbar navbar-static-top gray-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome to SWIFT-PAY.</span>
                </li>
                <li class="dropdown">
                    <?php if( auth()->check() ): ?>
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-user-circle-o"></i>
                            <?php echo e(auth()->user()->username); ?>

                            <span class="caret"></span>
                        </a>
                    <?php endif; ?>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a class="dropdown-item" href="<?php echo e(route('profile.edit')); ?>"><i class="fa fa-cog"></i>
                                Settings</a>
                            <a class="dropdown-item" href="<?php echo e(route('logout')); ?>"
                               onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i> Log Out
                            </a>

                            <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none">
                                <?php echo csrf_field(); ?>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <?php if($message = Session::get('success')): ?>
        <div class="alert-alert-success-alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <p><?php echo e($message); ?></p>
        </div>
    <?php endif; ?>
</div>
<?php /**PATH C:\laragon\www\payment-gateway\resources\views/layouts/topnavbar.blade.php ENDPATH**/ ?>