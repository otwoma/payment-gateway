<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <img alt="image" class="rounded-circle" src="<?php echo e(asset('images/payconnect-logo.png')); ?>"/>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="block m-t-xs font-bold">PayConnect &copy;</span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a class="dropdown-item" href="<?php echo e(route('profile.edit')); ?>"><i class="fa fa-user-circle"></i> Profile</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="<?php echo e(route('logout')); ?>"><i class="fa fa-sign-out"></i> Logout</a>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">
                    S-PAY
                </div>
            </li>
            <li class="<?php echo e(isActiveRoute('home')); ?>">
                <a href="<?php echo e(url('/home')); ?>"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
            </li>

            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('viewConfiguration')): ?>
                <li>
                    <a href="#">
                        <i class="fa fa-cogs"></i> <span class="nav-label">Configurations</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo e(url('/fees_configuration')); ?>"><i class="fa fa-money"></i> Fees Configuration</a></li>
                        <li><a href="<?php echo e(url('/permissions')); ?>"><i class="fa fa-key"></i> Permissions</a></li>
                        <li><a href="<?php echo e(url('/roles')); ?>"><i class="fa fa-user-secret"></i> Roles</a></li>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('viewUserManagement')): ?>
                <li class="<?php echo e(isActiveRoute('users')); ?>">
                    <a href="<?php echo e(url('/users')); ?>"><i class="fa fa-user-circle"></i>
                        <span class="nav-label">User Management</span>
                    </a>
                </li>
            <?php endif; ?>

            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('viewClientUsers')): ?>
                <li class="<?php echo e(isActiveRoute('client')); ?>">
                    <a href="<?php echo e(url('/client')); ?>"><i class="fa fa-user-plus"></i>
                        <span class="nav-label">Client Users</span>
                    </a>
                </li>
            <?php endif; ?>

            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('viewFinancialInstitution')): ?>
                <li class="<?php echo e(isActiveRoute('institution')); ?>">
                    <a href="<?php echo e(url('/institution')); ?>"><i class="fa fa-bank"></i>
                        <span class="nav-label">Financial Institution</span>
                    </a>
                </li>
            <?php endif; ?>
        </ul>

    </div>
</nav>
<?php /**PATH C:\laragon\www\payment-gateway\resources\views/layouts/navigation.blade.php ENDPATH**/ ?>