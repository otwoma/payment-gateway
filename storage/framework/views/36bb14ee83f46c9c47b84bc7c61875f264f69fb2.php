<div class="footer">
    <div class="pull-right">
        SWIFT-PAY
    </div>
    <div>
        <strong>Powered by</strong> Payconnect Ltd &copy; 2020-<?php echo date("Y"); ?>
    </div>
</div>
<?php /**PATH C:\laragon\www\payment-gateway\resources\views/layouts/footer.blade.php ENDPATH**/ ?>