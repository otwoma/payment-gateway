<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>

<body>
<h2>Welcome to Swift-Pay <?php echo e($name); ?></h2>
<br/>
Your registered email-id is <?php echo e($email); ?> , Please click on the below link to verify your email account
<br/>
<a href="<?php echo e(route('verify.user', $token)); ?>">Verify Email</a>
</body>

</html><?php /**PATH C:\laragon\www\payment-gateway\resources\views/emails/verifyUser.blade.php ENDPATH**/ ?>