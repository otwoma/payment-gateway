<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <img alt="image" class="rounded-circle" src="{{asset('images/payconnect-logo.png')}}"/>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="block m-t-xs font-bold">PayConnect &copy;</span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a class="dropdown-item" href="{{ route('profile.edit') }}"><i class="fa fa-user-circle"></i> Profile</a></li>
                        <li class="dropdown-divider"></li>
                        <li><a class="dropdown-item" href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> Logout</a>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">
                    S-PAY
                </div>
            </li>
            <li class="{{ isActiveRoute('home') }}">
                <a href="{{ url('/home') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
            </li>

            @can('viewConfiguration')
                <li>
                    <a href="#">
                        <i class="fa fa-cogs"></i> <span class="nav-label">Configurations</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="{{ url('/fees_configuration') }}"><i class="fa fa-money"></i> Fees Configuration</a></li>
                        <li><a href="{{ url('/permissions') }}"><i class="fa fa-key"></i> Permissions</a></li>
                        <li><a href="{{ url('/roles') }}"><i class="fa fa-user-secret"></i> Roles</a></li>
                    </ul>
                </li>
            @endcan

            @can('viewUserManagement')
                <li class="{{ isActiveRoute('users') }}">
                    <a href="{{ url('/users') }}"><i class="fa fa-user-circle"></i>
                        <span class="nav-label">User Management</span>
                    </a>
                </li>
            @endcan

            @can('viewClientUsers')
                <li class="{{ isActiveRoute('client') }}">
                    <a href="{{ url('/client') }}"><i class="fa fa-user-plus"></i>
                        <span class="nav-label">Client Users</span>
                    </a>
                </li>
            @endcan

            @can('viewFinancialInstitution')
                <li class="{{ isActiveRoute('institution') }}">
                    <a href="{{ url('/institution') }}"><i class="fa fa-bank"></i>
                        <span class="nav-label">Financial Institution</span>
                    </a>
                </li>
            @endcan
        </ul>

    </div>
</nav>
