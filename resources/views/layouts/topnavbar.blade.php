<div class="row border-bottom">
    <nav class="navbar navbar-static-top gray-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome to SWIFT-PAY.</span>
                </li>
                <li class="dropdown">
                    @if( auth()->check() )
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-user-circle-o"></i>
                            {{ auth()->user()->username }}
                            <span class="caret"></span>
                        </a>
                    @endif
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a class="dropdown-item" href="{{ route('profile.edit') }}"><i class="fa fa-cog"></i>
                                Settings</a>
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                                <i class="fa fa-sign-out"></i> Log Out
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    @if ($message = Session::get('success'))
        <div class="alert-alert-success-alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <p>{{ $message }}</p>
        </div>
    @endif
</div>
