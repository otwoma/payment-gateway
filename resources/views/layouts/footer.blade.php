<div class="footer">
    <div class="pull-right">
        SWIFT-PAY
    </div>
    <div>
        <strong>Powered by</strong> Payconnect Ltd &copy; 2020-<?php echo date("Y"); ?>
    </div>
</div>
