@extends('layouts.app')

@section('title', 'Permissions')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Permissions</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a>Permission Management</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Permission Registry</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>System Permissions</h5>
                        <div class="ibox-tools">
                            <a href="{{ route('permissions.create') }}"  data-target="#addPermModal" rel="tooltip"
                               data-placement="top" title="Add Permission">
                                <i class="fa fa-key"></i>
                                Add Permission
                            </a>
                            <a href="{{ route('roles.index') }}" rel="tooltip"
                               data-placement="top" title="View Roles">
                                <i class="fa fa-user-circle-o"></i>
                                Roles
                            </a>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="userDatatable">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Permission</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($permissions as $permission)
                                    <tr>
                                        <td>{{ $permission->id }}</td>
                                        <td>{{ $permission->name }}</td>
                                        <td>{{ $permission->description }}</td>
                                        <td class="text-right">
                                            @if($permission->id!=auth()->user()->id)
                                            <a href="{{ URL::to('permissions/'.$permission->id.'/edit') }}" title="Edit Permission"
                                               class="btn btn-success btn-sm" rel="tootltip" data-placement="top">
                                                <i class="fa fa-edit"></i>
                                            </a>

                                            <form action="{{ route('permissions.destroy', $permission) }}" method="post"
                                                  style="display:inline-block;" class="delete-form">
                                                @csrf
                                                @method('delete')
                                                <button type="button" rel="tooltip"
                                                        class="btn btn-danger btn-sm" title="Delete Permission" data-placement="top"
                                                        onclick="confirm('{{ __('Are you sure you want to delete this permission?') }}') ? this.parentElement.submit() : ''">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Permission</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection