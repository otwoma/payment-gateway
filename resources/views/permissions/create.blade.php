@extends('layouts.app')

@section('title', ' Create Permission')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Permissions</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a>Permission Management</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Add Permission</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5><i class='fa fa-plus-square-o'></i> Add Permission</h5>
                        <div class="ibox-tools">
                            <a href="{{ route('permissions.index') }}" rel="tooltip" data-placement="top" title="Back to Permissions">
                                <i class="fa fa-key"></i>
                                <strong>Permissions</strong>
                            </a>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        {{ Form::open(array('url' => 'permissions')) }}

                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('name', '', array('class' => 'form-control')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('name', 'Description') }}
                            {{ Form::text('name', '', array('class' => 'form-control')) }}
                        </div>
                        <br>
                        @if(!$roles->isEmpty())
                            <h4>Assign Permission to Roles</h4>

                            @foreach ($roles as $role)
                                {{ Form::checkbox('roles[]',  $role->id ) }}
                                {{ Form::label($role->name, ucfirst($role->name)) }}<br>

                            @endforeach
                        @endif
                        <br>
                        {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
