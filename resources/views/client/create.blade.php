@extends('layouts.app')

@section('title', 'Client Configuration')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Clients</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a>Client Management</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Add Client</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        @include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title">
                    <h5><i class='fa fa-plus-square-o'></i> Add User</h5>
                    <div class="ibox-tools">
                        <a href="{{ route('client.index') }}" rel="tooltip" data-placement="top"
                           title="Back to Roles">
                            <i class="fa fa-user-secret"></i>
                            <strong>Back to List</strong>
                        </a>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    {!! Form::open(array('url' => 'client', 'files' => true, 'method' => 'post')) !!}
                    <legend>Client Details:</legend>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('bname')) has-error @endif">
                                {!! Form::label('Business name', 'Business name') !!}
                                {!! Form::text('bname', '', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('reg_no')) has-error @endif">
                                {!! Form::label('Business Reg Number', 'Business Reg Number') !!}
                                {!! Form::text('reg_no', '', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>   
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('desc')) has-error @endif">
                                {!! Form::label('Business name', 'Description') !!}
                                {!! Form::text('desc', '', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('mobile_number')) has-error @endif">
                                {!! Form::label('Business Reg Number', 'Mobile-number') !!}
                                {!! Form::text('mobile_number', '', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('email')) has-error @endif">
                                {!! Form::label('Business name', 'Email') !!}
                                {!! Form::email('email', '', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('kra_pin')) has-error @endif">
                                {!! Form::label('Business Reg Number', 'Kra Pin') !!}
                                {!! Form::text('kra_pin', '', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('address')) has-error @endif">
                                {!! Form::label('Business name', 'Address') !!}
                                {!! Form::text('address', '', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>             

                    <legend>Contact-Person Details:</legend>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('fname')) has-error @endif">
                                {!! Form::label('First name', 'First name') !!}
                                {!! Form::text('fname', '', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('surname')) has-error @endif">
                                {!! Form::label('Surname', 'Surname') !!}
                                {!! Form::text('surname', '', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('other_name')) has-error @endif">
                                {!! Form::label('Other name', 'Other name') !!}
                                {!! Form::text('other_name', '', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('c_email')) has-error @endif">
                                {!! Form::label('Email', 'Email') !!}
                                {!! Form::text('c_email', '', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group">
                            <Label>Identification Document</Label>
                            <select id="id_type" name="id_type" class="form-control" required>
                                <option value="1">National Id</option>
                                <option value="2">Passport</option>
                                <option value="3">Military</option>
                                <option value="4">Alien</option>
                                <option value="5">Other</option>
                              </select>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('id_number')) has-error @endif">
                                {!! Form::label('Id', 'Id Number') !!}
                                {!! Form::text('id_number', '', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('c_mobile_number')) has-error @endif">
                                {!! Form::label('Mobile Number', 'Mobile Number') !!}
                                {!! Form::text('c_mobile_number', '', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('c_address')) has-error @endif">
                                {!! Form::label('Address', 'Address') !!}
                                {!! Form::text('c_address', '', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div> 
                    <legend>Document Upload | <small>business registration certificate</small></legend>
                    <div class="form-group">
                        <label for="upload_file" class="control-label col-sm-3">Upload File</label>
                        <div class="col-sm-9">
                             <input class="form-control" type="file" enctype="multipart/form-data" name="upload_file" id="upload_file" required>
                        </div>
                   </div>

                    {!! Form::submit('Add', array('class' => 'btn btn-primary')) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

