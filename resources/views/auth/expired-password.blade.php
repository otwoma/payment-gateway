@extends('layouts.app')

@section('title', ' Password Expired')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Users</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a>Expired Password</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Reset Password</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        @include('flash::message')

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5><i class='fa fa-plus-square-o'></i> Expired Password - Update</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>

                    <div class="ibox-content">
                        {!! Form::open(array('url' => 'expired-password')) !!}

                        <div class="form-group @if ($errors->has('current_password')) has-error @endif">
                            {!! Form::label('password', 'Current password') !!}<br>
                            {!! Form::password('current_password', array('class' => 'form-control')) !!}

                        </div>

                        <div class="form-group @if ($errors->has('new_password')) has-error @endif">
                            {!! Form::label('password', 'New Password') !!}<br>
                            {!! Form::password('new_password', array('class' => 'form-control')) !!}

                        </div>

                        <div class="form-group @if ($errors->has('new_password_confirmation')) has-error @endif">
                            {!! Form::label('password', 'Confirm New Password') !!}<br>
                            {!! Form::password('new_password_confirmation', array('class' => 'form-control')) !!}

                        </div>

                        {!! Form::submit('Update password', array('class' => 'btn btn-primary')) !!}

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection