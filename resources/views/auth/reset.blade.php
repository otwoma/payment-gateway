<!DOCTYPE html>
<html>
<head>
    <title>Password Reset Request</title>
</head>

<body>
<h2>Welcome to Swift-Pay</h2>
<br/>
Your registered email-id is {{$email}} , Please click on the below link to reset your password
<br/>
<a href="{{route('verify.passrequest', $token)}}">Reset Password</a>
</body>

</html>