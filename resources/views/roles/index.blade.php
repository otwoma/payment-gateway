@extends('layouts.app')

@section('title', ' Roles')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Roles</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a>Role Management</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Role Registry</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        @include('flash::message')
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5><i class="fa fa-user-secret"></i> User Roles</h5>
                        <div class="ibox-tools">
                            <a href="{{ route('roles.create') }}"  data-target="#addPermModal" rel="tooltip"
                               data-placement="top" title="Add Role">
                                <i class="fa fa-user-secret"></i>
                                Add Role
                            </a>
                            <a href="{{ route('permissions.index') }}" rel="tooltip"
                               data-placement="top" title="View Permissions">
                                <i class="fa fa-key"></i>
                                Permissions
                            </a>
                            <a href="{{ route('users.index') }}" rel="tooltip"
                               data-placement="top" title="View Users">
                                <i class="fa fa-user-circle-o"></i>
                                Users
                            </a>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Role</th>
                                    <th style="width: 85%;">Permissions</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach ($roles as $role)
                                    <tr>

                                        <td>{{ $role->name }}</td>

                                        <td>{{ str_replace(array('[',']','"'),' ', $role->permissions()->pluck('name')) }}</td>
                                        <td class="text-right">
                                            <a href="{{ URL::to('roles/'.$role->id.'/edit') }}" title="Edit Role"
                                               class="btn btn-success btn-sm" rel="tootltip" data-placement="top">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <form action="{{ route('roles.destroy', $role) }}" method="post"
                                                  style="display:inline-block;" class="delete-form">
                                                @csrf
                                                @method('delete')
                                                <button type="button" rel="tooltip"
                                                        class="btn btn-danger btn-sm" title="Delete Role" data-placement="top"
                                                        onclick="confirm('{{ __('Are you sure you want to delete this role?') }}') ? this.parentElement.submit() : ''">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection