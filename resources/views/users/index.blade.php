@extends('layouts.app')

@section('title', 'User Configuration')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Users</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a>User Management</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>User Configuration</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        @include('flash::message')
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Registered Users</h5>
                        <div class="ibox-tools">
                            @can('addUser')
                                <a href="{{ route('users.create') }}" data-target="#addUsrModal" rel="tooltip"
                                   data-placement="top" title="Add User">
                                    <i class="fa fa-user-plus"></i>
                                    Add User
                                </a>
                            @endcan
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="userDataTable">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Creation Date</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->username }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->roles()->pluck('name')->implode(' ') }}</td>
                                        <td>{{ $user->created_at->format('F d, Y h:ia') }}</td>
                                        <td class="text-right">
                                            @if($user->id!=auth()->user()->id)
                                                @can('editUser')
                                                    <a type="button" href="{{route("users.edit", $user)}}" rel="tooltip"
                                                       class="btn btn-success btn-sm" data-placement="top"
                                                       title="Edit User">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                @endcan
                                                <form action="{{ route('users.destroy', $user) }}" method="post"
                                                      style="display:inline-block;" class="delete-form">
                                                    @csrf
                                                    @method('delete')
                                                    @can('deleteUser')
                                                        <button type="button" rel="tooltip"
                                                                class="btn btn-danger btn-sm" title="Delete User"
                                                                data-placement="top"
                                                                onclick="confirm('{{ __('Are you sure you want to delete this user?') }}') ? this.parentElement.submit() : ''">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    @endcan
                                                </form>
                                            @else
                                                <a type="button" href="{{ route('profile.edit') }}" rel="tooltip"
                                                   class="btn btn-success btn-sm " data-original-title=""
                                                   title="Edit Profile">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Creation Date</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {
            $('#userDataTable').DataTable();
        });
    </script>
@endsection

