@extends('layouts.app')

@section('title', 'User Configuration')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Users</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a>User Management</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Edit User</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Edit User</h5>
                        <div class="ibox-tools">
                            <a href="{{ route('users.index') }}" rel="tooltip"
                               data-placement="top" title="Back to list">
                                <i class="fa fa-list"></i>
                                Back to list
                            </a>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT')) }}

                        <div class="form-group @if ($errors->has('username')) has-error @endif">
                            {{ Form::label('username', 'Username') }}
                            {{ Form::text('username', null, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group @if ($errors->has('firstname')) has-error @endif">
                            {{ Form::label('firstname', 'First Name') }}
                            {{ Form::text('first_name', null, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group @if ($errors->has('lastname')) has-error @endif">
                            {{ Form::label('lastname', 'Last Name') }}
                            {{ Form::text('last_name', null, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                            {{ Form::label('email', 'Email') }}
                            {{ Form::email('email', null, array('class' => 'form-control')) }}
                        </div>

                        <h5><b>Give Role</b></h5>

                        <div class="form-group @if ($errors->has('roles')) has-error @endif">
                            @foreach ($roles as $role)
                                {{ Form::checkbox('roles[]',  $role->id, $user->roles ) }}
                                {{ Form::label($role->name, ucfirst($role->name)) }}<br>

                            @endforeach
                        </div>

                        <div class="form-group @if ($errors->has('password')) has-error @endif">
                            {{ Form::label('password', 'Password') }}<br>
                            {{ Form::password('password', array('class' => 'form-control')) }}

                        </div>

                        <div class="form-group @if ($errors->has('password')) has-error @endif">
                            {{ Form::label('password', 'Confirm Password') }}<br>
                            {{ Form::password('password_confirmation', array('class' => 'form-control')) }}

                        </div>

                        {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
