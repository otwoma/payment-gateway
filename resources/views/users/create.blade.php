@extends('layouts.app')

@section('title', 'Add New User')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Users</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a>User Management</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Add User</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5><i class='fa fa-plus-square-o'></i> Add User</h5>
                        <div class="ibox-tools">
                            <a href="{{ route('users.index') }}" rel="tooltip" data-placement="top"
                               title="Back to Roles">
                                <i class="fa fa-user-secret"></i>
                                <strong>Back to List</strong>
                            </a>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        {!! Form::open(array('url' => 'users')) !!}

                        <div class="form-group @if ($errors->has('name')) has-error @endif">
                            {!! Form::label('name', 'Username') !!}
                            {!! Form::text('username', '', array('class' => 'form-control')) !!}
                        </div>

                        <div class="form-group @if ($errors->has('firstname')) has-error @endif">
                            {!! Form::label('name', 'First Name') !!}
                            {!! Form::text('first_name', '', array('class' => 'form-control')) !!}
                        </div>

                        <div class="form-group @if ($errors->has('lastname')) has-error @endif">
                            {!! Form::label('name', 'Last Name') !!}
                            {!! Form::text('last_name', '', array('class' => 'form-control')) !!}
                        </div>

                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                            {!! Form::label('email', 'Email') !!}
                            {!! Form::email('email', '', array('class' => 'form-control')) !!}
                        </div>

                        <div class="form-group @if ($errors->has('roles')) has-error @endif">
                            @foreach ($roles as $role)
                                {!! Form::checkbox('roles[]',  $role->id ) !!}
                                {!! Form::label($role->name, ucfirst($role->name)) !!}<br>

                            @endforeach
                        </div>

                        <div class="form-group @if ($errors->has('password')) has-error @endif">
                            {!! Form::label('password', 'Password') !!}<br>
                            {!! Form::password('password', array('class' => 'form-control')) !!}

                        </div>

                        <div class="form-group @if ($errors->has('password')) has-error @endif">
                            {!! Form::label('password', 'Confirm Password') !!}<br>
                            {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}

                        </div>

                        {!! Form::submit('Add', array('class' => 'btn btn-primary')) !!}

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection