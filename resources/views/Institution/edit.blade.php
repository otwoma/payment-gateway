@extends('layouts.app')

@section('title', 'Institution')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Financial Institutions</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Institution Configuration</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        @include('flash::message')
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5><i class="fa fa-plus-square-o"></i> Edit Institution</h5>
                        <div class="ibox-tools">
                            <a href="{{ route('institution.index') }}" data-toggle="modal" rel="tooltip"
                               data-placement="top" title="Back to Institution">
                                <i class="fa fa-bank"></i>
                                <strong>Institution</strong>
                            </a>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        {!! Form::model($institution, array('route' => array('institution.update', $institution->id), 'method' => 'PUT')) !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @if ($errors->has('companyName')) has-error @endif">
                                    {!! Form::label('companyName', 'Company Name') !!}
                                    {!! Form::text('name', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if ($errors->has('registrationNumber')) has-error @endif">
                                    {!! Form::label('registrationNumber', 'Registration Number') !!}
                                    {!! Form::text('company_reg_no', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @if ($errors->has('email')) has-error @endif">
                                    {!! Form::label('email', 'Email Address') !!}
                                    {!! Form::text('email', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if ($errors->has('phoneNumber')) has-error @endif">
                                    {!! Form::label('phoneNumber', 'Phone Number') !!}
                                    {!! Form::text('phone_number', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @if ($errors->has('krapin')) has-error @endif">
                                    {!! Form::label('krapin', 'KRA PIN') !!}
                                    {!! Form::text('kra_pin', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @if ($errors->has('physicalAddress')) has-error @endif">
                                    {!! Form::label('physicalAddress', 'Physical Address') !!}
                                    {!! Form::text('physical_address', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if ($errors->has('postalAddress')) has-error @endif">
                                    {!! Form::label('postalAddress', 'Postal Address') !!}
                                    {!! Form::text('postal_address', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group @if ($errors->has('postalAddress')) has-error @endif">
                                    {!! Form::label('description', 'Description') !!}
                                    {!! Form::textarea('description', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>
                        </div>
                        {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection