@extends('layouts.app')

@section('title', 'Institution')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Financial Institutions</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Institution Configuration</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        @include('flash::message')

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5><i class="fa fa-bank"></i> Registered Institutions</h5>
                        <div class="ibox-tools">
                            <a href="{{ route('institution.create') }}" data-toggle="modal" rel="tooltip"
                               data-placement="top" title="Add Institution">
                                <i class="fa fa-plus-square"></i>
                                Add Institution
                            </a>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="userDatatable">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Physical Address</th>
                                    <th>Creation Date</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($institutions as $institution)
                                    <tr>
                                        <td>{{ $institution->id }}</td>
                                        <td>{{ $institution->name }}</td>
                                        <td>{{ $institution->email }}</td>
                                        <td>{{ $institution->physical_address }}</td>
                                        <td>{{ $institution->created_at->format('F d, Y h:ia') }}</td>
                                        <td class="text-right">
                                            @if($institution->id!=auth()->user()->id)
                                                <a href="{{ URL::to('institution/'.$institution->id.'/edit') }}" title="Edit Institution"
                                                   class="btn btn-success btn-sm" rel="tootltip" data-placement="top">
                                                    <i class="fa fa-edit"></i>
                                                </a>

                                                <form action="{{ route('institution.destroy', $institution) }}" method="post"
                                                      style="display:inline-block;" class="delete-form">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="button" rel="tooltip"
                                                            class="btn btn-danger btn-sm" title="Delete Institution" data-placement="top"
                                                            onclick="confirm('{{ __('Are you sure you want to delete this institution?') }}') ? this.parentElement.submit() : ''">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Physical Address</th>
                                    <th>Creation Date</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">--}}
    {{--<div class="modal-dialog">--}}
    {{--<div class="modal-content">--}}
    {{--<div class="modal-header">--}}
    {{--<h4 class="modal-title" id="myModalLabel"><i class="fa fa-user-plus"></i> Add Institution </institution {{--</div>--}}
    {{--<div class="modal-body">--}}
    {{--<h3>Registration form here</h3>--}}
    {{--</div>--}}
    {{--<div class="modal-footer">--}}
    {{--<button type="button" class="btn btn-default btn-rounded" data-dismiss="modal">Close</button>--}}
    {{--<button type="button" class="btn btn-primary btn-rounded">Save changes</button>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
@endsection

