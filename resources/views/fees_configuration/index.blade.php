@extends('layouts.app')

@section('title', 'Fee Configurations')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Users</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a>Configurations</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Fees Configuration</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        @include('flash::message')

        <div class="row">
            <div class="col-md-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Fee Groups</h5>
                        <div class="ibox-tools">
                            <a href="#" data-toggle="modal" data-target="#addFeeModal" rel="tooltip" data-placement="top" title="Add Fee">
                                <i class="fa fa-money"></i>
                                Add Fee Group
                            </a>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="userDataTable">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Group Name</th>
                                    <th>Fee(s)</th>
                                    <th>% Charge</th>
                                    <th>Creation Date</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($fees as $fee)
                                    <tr>
                                        <td>{{ $fee->id }}</td>
                                        <td>{{ $fee->group_name }}</td>
                                        <td>{{ $fee->fee }}</td>
                                        <td>{{ $fee->percent }}</td>
                                        <td>{{ $fee->created_at->format('F d, Y h:ia') }}</td>
                                        <td class="text-right">
                                            @if($fee->id!=auth()->user()->id)
                                                <a href="{{ URL::to('fees_configuration/'.$fee->id.'/edit') }}" title="Edit Institution"
                                                   class="btn btn-success btn-sm" rel="tootltip" data-placement="top">
                                                    <i class="fa fa-edit"></i>
                                                </a>

                                                <form action="{{ route('fees_configuration.destroy', $fee) }}" method="post"
                                                      style="display:inline-block;" class="delete-form">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="button" rel="tooltip"
                                                            class="btn btn-danger btn-sm" title="Delete Group" data-placement="top"
                                                            onclick="confirm('{{ __('Are you sure you want to delete this group?') }}') ? this.parentElement.submit() : ''">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Group Name</th>
                                    <th>Fee(s)</th>
                                    <th>% Charge</th>
                                    <th>Creation Date</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="addFeeModal" class="modal fade animated fadeInDown" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-money"></i> New Fee Group</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ 'fees_configuration.store' }}" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="feeGroup">Group Name</label>
                                    <input type="text" class="form-control" name="group_name" id="feeGroup"
                                           value="{{ old('group_name') }}" autocomplete="off" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fee">Fee</label>
                                    <input type="number" class="form-control" name="fee" id="fee"
                                           value="{{ old('fee') }}" autocomplete="off" required>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="rate">Percentage</label>
                                    <input type="text" class="form-control" name="percent" id="percentage"
                                           autocomplete="off" value="{{ old('percentage') }}" required>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>

        </div>
    </div>
@endsection