@extends('layouts.app')

@section('title', 'Fee Configurations')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Users</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Edit Fees Group</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        @include('flash::message')
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5><i class="fa fa-plus-square-o"></i> Edit Fee Group</h5>
                        <div class="ibox-tools">
                            <a href="{{ route('fees_configuration.index') }}" data-toggle="modal" rel="tooltip"
                               data-placement="top" title="Back to List">
                                <i class="fa fa-money"></i>
                                <strong>Fee Groups</strong>
                            </a>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        {!! Form::model($fee, array('route' => array('fees_configuration.update', $fee->id), 'method' => 'PUT')) !!}

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group @if ($errors->has('groupName')) has-error @endif">
                                    {!! Form::label('groupName', 'Group Name') !!}
                                    {!! Form::text('group_name', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group @if ($errors->has('feeCharge')) has-error @endif">
                                    {!! Form::label('feeCharge', 'Fees') !!}
                                    {!! Form::text('fee', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group @if ($errors->has('percentage')) has-error @endif">
                                    {!! Form::label('percentage', 'Percentage') !!}
                                    {!! Form::number('percent'), null, array('class' => 'form-control') !!}
                                </div>
                            </div>
                        </div>
                        {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection