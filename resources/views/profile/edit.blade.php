@extends('layouts.app')

@section('title', ' Profile')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>User Profile</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    Settings
                </li>
                <li class="breadcrumb-item active">
                    <strong>Profile</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        @include('flash::message')
        <div class="row">
            <div class="col-md-8">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Edit Profile</h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" action="{{ route('profile.upload') }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <input type="file" class="form-control-file" name="avatar" id="avatarFile"
                                       aria-describedby="fileHelp">
                                {{--@if(auth()->user()->avatar)--}}
                                {{--<code>{{ auth()->user()->avatar }}</code>--}}
                                {{--@endif--}}
                                <small id="fileHelp" class="form-text text-muted">
                                    Please upload a valid image file. Size of image should not be more than 2MB.
                                </small>
                            </div>
                            <div class="dt-button-info">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i>
                                    Upload Photo
                                </button>
                            </div>
                            <br>
                        </form>
                        <form method="post" action="{{ route('profile.update') }}" autocomplete="off"
                              enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="row">

                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label>{{__(" Username")}}</label>
                                        <input type="text" name="username" class="form-control"
                                               value="{{ old('name', auth()->user()->username) }}">
                                        @include('alerts.feedback', ['field' => 'name'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">{{__(" Email address")}}</label>
                                        <input type="email" name="email" class="form-control" placeholder="Email"
                                               value="{{ old('email', auth()->user()->email) }}">
                                        @include('alerts.feedback', ['field' => 'email'])
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-footer ">
                                <button type="submit" class="btn btn-primary btn-round"><i
                                            class="fa fa-share"></i>{{__(' Save')}}
                                </button>
                            </div>
                        </form>
                    </div>
                    <br>

                    <div class="ibox-title">
                        <h5>Password</h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" action="{{ route('profile.password') }}" autocomplete="off">
                            @csrf
                            @method('put')
                            @include('alerts.success', ['key' => 'password_status'])
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <label>{{__(" Current Password")}}</label>
                                        <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                               name="old_password" placeholder="{{ __('Current Password') }}"
                                               type="password" required>
                                        @include('alerts.feedback', ['field' => 'old_password'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <label>{{__(" New password")}}</label>
                                        <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                               placeholder="{{ __('New Password') }}" type="password" name="password"
                                               required>
                                        @include('alerts.feedback', ['field' => 'password'])
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-7 pr-1">
                                    <div class="form-group {{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <label>{{__(" Confirm New Password")}}</label>
                                        <input class="form-control" placeholder="{{ __('Confirm New Password') }}"
                                               type="password" name="password_confirmation" required>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-footer ">
                                <button type="submit" class="btn btn-primary btn-round ">
                                    Change Password
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>User Photo</h5>
                    </div>
                    <div class="ibox-content">
                        <a href="#">
                            <img class="card-img-bottom" src="{{asset(auth()->user()->avatar) }}" alt="...">
                        </a>
                        <div>
                            <label for="email">Username:</label>
                            {{ auth()->user()->username }}
                        </div>

                        <div>
                            <label for="email">Email:</label>
                            {{ auth()->user()->email }}
                        </div>
                        <div>
                            <label for="role">Role:</label>
                            {{ user()->roles()->pluck('name') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection