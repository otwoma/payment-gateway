@extends('layouts.app')

@section('title', 'Change Password')

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Settings</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('home') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Change Password</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        @include('flash::message')

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5><i class='fa fa-lock'></i> Change Password</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        {!! Form::open(array('url' => 'update-password')) !!}

                        <div class="form-group @if ($errors->has('password')) has-error @endif">
                            {!! Form::label('password', 'Password') !!}<br>
                            {!! Form::password('password', array('class' => 'form-control')) !!}

                        </div>

                        <div class="form-group @if ($errors->has('password')) has-error @endif">
                            {!! Form::label('password', 'Confirm Password') !!}<br>
                            {!! Form::password('password_confirmation', array('class' => 'form-control')) !!}

                        </div>

                        {!! Form::submit('Change password', array('class' => 'btn btn-primary')) !!}

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection