<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


/**
 *
 * Login view Route(s)
 */
Route::get('/', function () {
    return view('login');
});

Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser')->name('verify.user');

Auth::routes(['verify' => true]);

/**
 * Login Route(s)
 */
Route::get('login', 'AuthController@login')->name('login');
Route::post('login', 'AuthController@loginProcess');
Route::get('logout', 'AuthController@logout')->name('logout');

/**
 * OTP Route(s)
 */
#Route::post('verifyOTP/{otp}', 'AuthController@verifyOTP');
Route::post('verifyOTP', 'AuthController@verifyOTP')->name('verify-otp');

/**
 * Password Reset Link Route(s)
 */
Route::get('email', 'ForgotPassController@showRequestForm')->name('email');
Route::post('email', 'ForgotPassController@sendResetLinkEmail');

/**
 * Password Reset Route(s)
 */
Route::get('reset/{token}', 'ForgotPassController@verifyPassRequest')->name('verify.passrequest');
Route::post('reset', 'ForgotPassController@resetPass')->name('reset');

/**
 * Password Expiration Route(s)
 */
Route::get('expired-password','PwdExpirationController@showPasswordExpirationForm')->name('passwordExpiration');
Route::post('expired-password','PwdExpirationController@postPasswordExpiration');

/**
 * Change Password Route(s)
 */
Route::get('change-password', 'AuthController@passChangeForm')->name('change-password');
Route::post('update-password', 'AuthController@updatePassword');

/**
 * Dashboard Route
 */
Route::get('/home', 'HomeController@index')->name('home');

Route::group( ['middleware' => ['auth', 'has_permission', 'verified']], function() {
    Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController');
    Route::resource('permissions','PermissionController');

    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
    Route::put('profile/upload', ['as' => 'profile.upload', 'uses' => 'ProfileController@upload']);

    Route::resource('institution', 'InstitutionController');
    Route::resource('client', 'ClientController');
    
    
});

Route::get('/verification/phonenumber', 'Auth\RegisterController@generateRandomSecurityKey');

/**
 * Client Configurations Route(s)
 */
Route::group(['middleware' => ['auth', 'has_permission', 'verified'],['prefix' => 'Fees Configuration']], function () {
    Route::resource('fees_configuration', 'FeesController');
});

/**
 * Error Logs
 */
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

